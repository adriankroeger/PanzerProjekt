bisherige Klassen:

+ Feld
+ Panzer
+ Hindernis
+ Projektil
+ Powerup
+ Spieler
+ GUI

ToDo:
+ UML-Diagramm erstellen
+ Klassendiagramm erstellen (Methoden etc.)


Milestones:
+ funktionierendes Gui
+ 32Ticks
+ fahrende Panzer
+ schiessende Panzer
+ eine Map mit Hindernissen
+ zerstoerbare Panzer 
+ zerstoerbare Hindernisse
+ Multiplayermodus


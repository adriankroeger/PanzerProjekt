/**
 * Bitte die Klasse erklaeren und euren Namen dazu schreiben.
 * Erstelle by Jannis :)
 * Der Spieler führt die Methoden des Panzers aus. Er hat eine ID und ein Punktesystem
 * Am Anfang sollte es vielleicht möglich sein, dem Spieler einen Namen zu geben
 */
public class Spieler {

  private Panzer panzer;
  private int punkte = 0; //Evtl. für mögliche mehrere Spiele
  private int spielerID = 0; //Alle Spieler haben eine ID (1-4), damit sie aufgeruen werden können, ausgeführt werden können
  //Ist die ID = 0, existiert der Spieler offiziell nicht
  private String name = "";
  private int spawnX = 0;
  private int spawnY = 0;

  private long respawn;

  public Spieler(int ID, int x, int y) {
    spielerID = ID;  // gimme dem Konstruktor, yo
    spawnX = x;
    spawnY = y;
  }

  public void Koordinaten(int x, int y) {
    spawnX = x;
    spawnY = y;
  }

  public Panzer getPanzer() { //Übergibt einen Panzer
    return panzer;
  }

  public void setPanzer(Panzer p) { //Übergibt dem Spieler einen Panzer
    panzer = p;
  }

  public String getName() {  //Übergibt den Namen, evtl. für Endscreen "Adrikan hat mit 42 Punkten gewonnen"
    return name;
  }

  public void setName(String s) {  //Gibt dem Spieler einen Namen
    name = s;
  }

  public int getPunkte() { //Übergibt Punkte, evtl. für Endscreen "Adrikan hat mit 420 Punkten gewonnen"
    return punkte;
  }

  public void setPunkte(int p) { //Verändert die Punkte
    punkte = p;
  }

  public void addPunkte(int p) {
    punkte += p; //Erhöht oder Verkleinert den Punktestand
  }

  public void vorwaerts() {
    panzer.vorwaerts(); //Panzer soll vorwärts gehen    
  }

  public void rueckwaerts() {
    panzer.rueckwaerts();//Panzer soll rückwärts gehen.    
  }

  public void rollen() {
    panzer.rolleAus();
  }

  public void linksDrehen() {
    panzer.lenkeLinks();//Panzer soll nach links drehen    
  }

  public void rechtsDrehen() {
    panzer.lenkeRechts();//Panzer soll nach rechts drehen    
  }

  public void feure() {
    panzer.feure(); //Lässt den Panzer feuern
  }

  public int getSpawnX() {
    return spawnX;
  }

  public int getSpawnY() {
    return spawnY;
  }

  public long getRespawn() {
    return respawn;
  }

  public void setRespawn(long respawn) {
    this.respawn = respawn;
  }
}

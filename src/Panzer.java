/*
Verwaltet alle Panzer
verfasst von Niklas Schlüter

Eine der drei Methoden "vorwaerts", "rueckwaerts" oder "rolleAus" sollte jeden Tick ausgeführt werden.
Welche das ist, wird durch Tasteneingabe entschieden.
Die korrekte Methode wird, genau wie die "drehe"-Methoden, von außerhalb ausgeführt.

Die Höchstgeschwindigkeit vmax sollte zu Beginn für alle Panzer gleich sein und nur durch Power-Ups
verändert werden.
Der Panzer erreicht 50% von vmax bei Start von 0 nach 17,5*m Durchläufen und 90% nach 57,5*m.

Bei jedem Aufruf der Methoden zum Bewegen (also in jedem Tick) wird auch der Cooldown, falls nötig,
heruntergesetzt.
*/

import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

@SuppressWarnings("WeakerAccess")
public class Panzer extends BewegtesObjekt {

  // Attribute
  private final double bm = Math.toRadians(2);     // Bogenmaß von 2°, grundlegend für alle Drehungen
  private final double f0 = Math.toRadians(3);     // Grenze für Subtraktion von der Richtung
  private final double f2p = Math.toRadians(357);  // Grenze für Addition auf die Richtung
  private final double pi = Math.PI;               // Pi, soll evtl. Laufzeit verbessern (?)
  private final int acc;     // Koeffizient für die Beschleunigung -> NICHT Beschleunigung im physikalischen Sinne
  private final int hpmax;   // HP-Wert ohne jegliche Beschädigung
  private final int ammomax; // Maximal mitführbare Munition
  private double vmax;       // Höchstgeschwindigkeit
  private double vp;         // Basisgeschwindigkeit geschossener Projektile
  private int cdmax;         // Länge eines Cooldowns in Ticks
  private int hp;            // verbleibende "Gesundheit"
  private int ammo;          // verbleibende Schüsse
  private int cd;            // aktueller Cooldown für Schussfunktion
  private int m;             // Gewichtsklasse (1 für leicht bis 3 für schwer)
  private Spieler fahrer;    // steuernder Spieler

  // Konstruktor
  public Panzer(int gewichtsklasse, int posx, int posy, Spieler s) {
    // Aufruf des Konstruktors für die Oberklasse:
    super(posx + 25, posy + 30, 0, 0);
    // Erstellung der Hitbox:
    hb = new Rectangle2D.Double(posx, posy, 40, 60);   // beispielhaft: Panzer 100 Punkte breit und hoch
    // Initialisierung aller Attribute:
    m = gewichtsklasse;
    fahrer = s;
    acc = GUI.getnAcc() * m;
    vmax = GUI.getnVmax();
    vp = vmax * GUI.getnVpmult();
    ammomax = GUI.getnAmmo();
    ammo = ammomax;
    hpmax = GUI.getnHpmult() * (3 + m);
    hp = hpmax;
    cdmax = GUI.getnCdmax();
    cd = 0;
  }

  // beschleunigt Panzer nach vorne (vom Spieler angefordert)
  public void vorwaerts() {
    v = v + (vmax - v) / acc;
    bewege();
    if (cd > 0) cd--;
  }

  // beschleunigt Panzer nach hinten (vom Spieler angefordert)
  public void rueckwaerts() {
    v = v - (vmax + v) / acc;
    bewege();
    if (cd > 0) cd--;
  }

  // lässt Panzer ausrollen (ausgeführt, wenn weder vor- noch rückwärts beschleunigt wird)
  public void rolleAus() {
    v = v - v / acc;
    bewege();
    if (cd > 0) cd--;
  }

  // führt sofortige Drehung um 2° nach rechts aus (vom Spieler angefordert; Betrag des Winkels hardcoded,
  // weil Veränderung direkte Konsequenzen für die Spielbarkeit hätte, Test und Anpassung stehen noch aus)
  public void lenkeLinks() {
    if (r > f0) r -= bm;
    else r = 2 * pi;
    AffineTransform t = new AffineTransform();
    t.rotate(-bm, xpos, ypos);
    hb = t.createTransformedShape(hb);
  }

  // führt sofortige Drehung um 2° nach rechts aus (vom Spieler angefordert; Betrag des Winkels hardcoded,
  // weil Veränderung direkte Konsequenzen für die Spielbarkeit hätte, Test und Anpassung stehen noch aus)
  public void lenkeRechts() {
    if (r < f2p) r += bm;
    else r = 0;
    AffineTransform t = new AffineTransform();
    t.rotate(bm, xpos, ypos);
    hb = t.createTransformedShape(hb);
  }

  // erstellt ein Projektil vor dem Lauf des Panzers mit Geschwindigkeitsbonus basierend auf aktueller Geschwindigkeit
  // des Panzers und Flugrichtung identisch zur Fahrtrichtung; beschleunigt den Panzer nach hinten (Rückstoß)
  public void feure() {
    if ((cd == 0) && (ammo > 0)) {
      double pxpos = xpos + Math.sin(r) * 35;   //
      double pypos = ypos - Math.cos(r) * 35;   //
      double fluggeschw = vp + v / 3;
      v -= vmax / 10;
      cd = cdmax;
      ammo--;
      Main.getFeld().addObject(new Projektil(pxpos, pypos, fluggeschw, r, m, fahrer));
    }
  }

  // reduziert hp um schaden und gibt zurück, ob Panzer zerstört ist
  public boolean nimmSchaden(int schaden) {
    hp -= schaden;
    return hp < 1;
  }

    // reduziert hp um ein Vielfaches des Geschwindigkeitsbetrag, wenn längere Beschleunigung vorliegt
    // gibt zurück, ob Panzer zerstört ist
    public boolean nimmKollSchaden(int koeff) {
      if (Math.abs(v) > vmax / acc) hp -= Math.abs(v) * koeff;
      return hp < 1;
    }

  // "heilt" den Panzer soweit möglich, ohne hpmax zu überschreiten
  public void wiederherstellen(int hpplus) {
    hp += hpplus;
    if (hp > hpmax) hp = hpmax;
  }

  // fügt soweit möglic weitere Munition hinzu, ohne ammomax zu überschreiten
  public void nachladen(int anz) {
    ammo += anz;
    if (ammo > ammomax) ammo = ammomax;
  }

  // führt nach einer Kollision notwendige Schritte durch, um ein Feststecken des Panzers zu verhindern
  public void zuruecksetzen() {
    r += pi;
    bewege();
    r -= pi;
    v = 0;
  }

    // gibt Werte für die Stats-Klasse
    public int[] werteAnzeigen() {
      int[] rg = new int[8];
      rg[0] = (int) Math.round(v * 10);         // Genauigkeit von einer Nachkommastelle gewünscht
      rg[1] = (int) (Math.abs(v) * 100 / vmax); // realisiert Skala von 0-100, entsprechend der ProgressBar
      rg[2] = hp;
      rg[3] = hp * 100 / hpmax;
      rg[4] = ammo;
      rg[5] = ammo * 100 / ammomax;
      rg[6] = Math.round(cd / 5);      // Anzeige von Zehntelsekunden
      rg[7] = 100 - cd * 100 / cdmax;  // Soll voll sein, wenn cd = 0; nach dem Feuern leer -> lädt sich sozusagen auf
      return rg;
    }

  // get- und set-Methoden:
  public double getVmax() {
    return vmax;
  }

  public void setVmax(double hoechstgeschw) {
    vmax = hoechstgeschw;
  }

  public Spieler getFahrer() {
    return fahrer;
  }

  public int getMasse() {
    return m;
  }

  public int getHp() {
    return hp;
  }

  public int getCdmax() {
    return cdmax;
  }

  public void setCdmax(int maxcd) {
    cdmax = (maxcd != 0) ? maxcd : 1; //wenn der übergebene wert 0 ist, wird er auf 1 gesetzt, da durch diesen wert geteilt wird
  }

}
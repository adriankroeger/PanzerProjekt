/**
 * Bitte die Klasse erklaeren und euren Namen dazu schreiben.
 */
class Main {

  public static final int WIDTH = 1500; //Breite des Feldes
  public static final int HEIGHT = 1000; //Höhe des Feldes
  public static final long RESPAWN = 2000; //Respawnzeit in Millisekunden
  private static Feld feld;

  public static void main(String[] args) {
    feld = new Feld(HEIGHT, WIDTH);
    new GUI();  // GUI mit dem Fenster wird gestartet.
  }

  public static Feld getFeld() {
    return feld;
  }
}
/*
Verwaltet alle Projektile
verfasst von Niklas Schlüter

Aufruf des Konstruktors erfolgt über die Klasse Panzer.
Bewegung des Projektils sollte jeden Tick von außen angestoßen werden.+
*/

import java.awt.geom.Ellipse2D;

public class Projektil extends BewegtesObjekt {

  // Attribute
  private int typ;  // Geschosstyp: wie Gewichtsklasse bei Panzer
  private Spieler schuetze;

  // Kostruktor
  public Projektil(double posx, double posy, double geschwindigkeit, double richtung, int geschossart, Spieler s) {
    // Aufruf des Konstruktors für die Oberklasse:
    super(posx + 10, posy + 10, geschwindigkeit, richtung);
    // Erstellung der Hitbox:
    hb = new Ellipse2D.Double(posx - 4, posy - 4, 9, 9);
    // Initialisierung aller Attribute:
    typ = geschossart;
    schuetze = s;
  }

  // get-Methoden:
  public Spieler getSchuetze() {
    return schuetze;
  }

  public int getSchaden() {
    return 50 + typ * 50;
  }

  public int getTyp() {
    return typ;
  }

}
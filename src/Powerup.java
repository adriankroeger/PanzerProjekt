import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.io.File;

/*
Klasse Powerup;
erstellt by Jannis
Die Klasse Powerup hat eine Id, die die Art der Powerups angiebt
Ausfuehren wird jeden Tick ausgefuehrt, dauer wird um jedes mal verkleinert, bis dauer = 0, bedeutet Powerup ist aufgebraucht

Id :
1 = Speed
2 = Schneller Schiessen
3 = Heilung
...

*/
public class Powerup extends ObjektMitHitbox {

  private int id = 0;
  private int dauer = 0;
  private int dauermax = 0;
  private Panzer p;
  private String respath = System.getProperty("user.dir").replace("PanzerProjekt","").replace("\\src", "") + "\\PanzerProjekt\\resources\\";
  private double i;
  private int j;

  public Powerup(int i, int time, double x, double y) {  //Beim erstellen muessen die Koordinaten und die Art uebergeben werden
    super(x, y);
    hb = new Rectangle2D.Double(x, y, 20, 20);
    id = i;         //Koordinaten und id wird gesetzt
    dauer = time;   //Dauer des Powerup wird gesetzt
    dauermax = time;
  }


  public int getId() {  //Id wird uebergeben
    return id;
  }

  public void setPanzer(Panzer p) {
    this.p = p;
  }

  public boolean ausfuehren() { //wird jeden Tick vom Feld aufgerufen
    switch (id) {

      case 1: {
        if (dauer < 1) {
          p.setVmax(i);           //Vmax wird wieder zurueckgegeben
          return true;
        } else {
          if (dauer == dauermax) {
            i = p.getVmax();   //Vmax wird gespeichert
          }
          p.setVmax(i * 1.3);    //Vmax wird für die Dauer des Powerups erhoeht
          dauer--;
          return false;
        }
      }

      case 2: {
        if (dauer < 1) {
          p.setCdmax(j);           //Cdmax wird wieder zurueckgegeben
          return true;
        } else {
          if (dauer == dauermax) {
            j = p.getCdmax();   //Cdmax wird gespeichert
          }
          p.setCdmax(j / 2);    //Cdmax wird für die Dauer des Powerups erhoeht
          dauer--;
          return false;
        }
      }

      case 3: {
        dauer--;
        p.wiederherstellen(1);   //Health wird jeden Tick erhoeht
        return dauer < 1;
      }

    }
    return true;
  }

  public Image getImage() throws Exception{
    switch(id) {
      case 1: return ImageIO.read(new File(respath + "Powerup_Arrow.png"));
      case 2: return ImageIO.read(new File(respath + "Powerup_Ammo.png"));
      case 3: return ImageIO.read(new File(respath + "Powerup_Heart.png"));
    }
    return ImageIO.read(new File(getClass().getResource("Powerup_Arrow.png").getPath()));
  }
}
  
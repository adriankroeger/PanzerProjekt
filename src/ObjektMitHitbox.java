/*
Oberklasse für alle Objekte auf dem Feld mit definierten Koordinaten und Hitbox
verfasst von Niklas Schlüter
*/

import java.awt.*;

public class ObjektMitHitbox {

  // Attribute
  protected Shape hb;
  protected double xpos;   // x-Koordinate auf Feld
  protected double ypos;   // y-Koordinate auf Feld

  // Konstruktor
  public ObjektMitHitbox(double posx, double posy) {
    xpos = posx;
    ypos = posy;
  }

  // get-Methoden:
  public Shape getHitbox() {
    return hb;
  }

  public int getXPos() {
    return (int) xpos;
  }

  public int getYPos() {
    return (int) ypos;
  }

}  
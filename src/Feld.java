import java.awt.*;
import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * Verwaltet das begrenzte Spielfeld auf dem sich Panzer bewegen und Hindernisse stehen.
 * Kollisionsabfragen und weitere Spielmechaniken werden hier verwaltet.
 * Verfasser: Adrian Kröger
 * _ _ _ _ _ _ _ _
 * |               |   Hoehe: h
 * |               |   Breite: b
 * |               |
 * |               |
 * |_ _ _ _ _ _ _ _|
 */

public class Feld {
  private int h;  //Hoehe des Spielfeldes
  private int b;  //Breite des Spielfeldes
  private Line2D[] borders = new Line2D[4]; //Array der Spielfeldbegrenzungen
  private List<ObjektMitHitbox> objects = new ArrayList<>(); //Liste aller Spielobjekte
  private List<BewegtesObjekt> moving = new ArrayList<>();
  private List<ObjektMitHitbox> destroyed = new ArrayList<>(); //Liste aller zerstörten Spielobjekte
  private List<BewegtesObjekt> movdestroyed = new ArrayList<>(); //Liste aller zerstörten Bewegten Objekte
  private List<Panzer> spawn = new ArrayList<>(); //Liste aller neu zu spawnenden Objekte
  private List<Projektil> projektils = new ArrayList<>(); //Liste aller derzeit unterwegsen projektile
  private List<Powerup> powerups = new ArrayList<>(); //Liste aller aktiven Powerups
  private long lastSpawned = 0;

  //Konstruktor der Feldklasse.
  public Feld(int hoehe, int breite) {
    h = hoehe;
    b = breite;
    init();
  }


  private void init() {
    borders[0] = new Line2D.Double(8, 0, 8, h); //Spielfeldbegrenzungen werden zu Linien gemacht
    borders[1] = new Line2D.Double(b, 0, b, h);
    borders[2] = new Line2D.Double(0, 30, b, 30);
    borders[3] = new Line2D.Double(0, h, b, h);

    //Falls es zu sehr laggt, können Sie ein paar Hindernisse entfernen

    //Map wird erstellt:
    for(int i = 0; i < 18; i++){ //große lange Mauer in der Mitte
      addObject(new Hindernis(100 + 40*i, 500));
    }
    addObject(new Hindernis(780, 540));
    addObject(new Hindernis(780, 580));
    addObject(new Hindernis(780, 620));
    addObject(new Hindernis(740, 620));
    addObject(new Hindernis(700, 620));
    addObject(new Hindernis(660, 620));
    addObject(new Hindernis(660, 580));
    addObject(new Hindernis(660, 660));
    for(int i = 0; i < 10; i++){
        addObject(new Hindernis(780 + 40*i,580));
    }
      addObject(new Hindernis(1140, 540));
      addObject(new Hindernis(1140, 500));
      addObject(new Hindernis(1140, 460));
      addObject(new Hindernis(1180, 460));
      addObject(new Hindernis(1220, 460));
      addObject(new Hindernis(1140, 620));
      addObject(new Hindernis(1140, 660));
      addObject(new Hindernis(1140, 700));
      addObject(new Hindernis(1180, 700));
      addObject(new Hindernis(1220, 700));
  }

  public void update() { //updated Spielgeschehnisse, wie powerupeffekte und projektile
    respawn(); //In jedem Tick werden Panzer mit abgelaufener respawnzeit respawnt
    for (Projektil p : projektils) { //in jedem tick werden die Projektile weiterbewegt
      p.bewege();
    }
    for (Powerup p : powerups) {
      if (p.ausfuehren()) { //die Effekte der aktiven powerups werden jeden tick ausgeführt, wenn powerup ausläuft, wird es aus der liste entfernt
        destroyed.add(p);
      }
    }
    powerups.removeAll(destroyed);//es funktioniert, da in diesem fall nur objekte des typen 'Powerup' in der Liste sind
    destroyed = new ArrayList<>();

    if(lastSpawned + 10000 < System.currentTimeMillis() && objects.size() <= 80){ //alle 10 sekunden wird ein neues Powerup gespawnt, und es werden keine gespawnt, wenn zu viele gegenstände vorhanden sind, um lag vorzubeugen
      lastSpawned = System.currentTimeMillis();

      Powerup neu = new Powerup((int)(Math.random()*3)+1, 500 - (-250 + (int)(Math.random() * 250) + 1), Math.random()* Main.WIDTH, Math.random()*Main.HEIGHT);
      //ein neues powerup mit zufälligen werten wird gespawnt
      for(int i = 0; i < objects.size(); i++){
        if(collides(neu.getHitbox(), objects.get(i).getHitbox())){ //wenn es dort spawnen würde, wo ein objelt steht, wird ein neues erschaffen, mit neuen werten
          addObject(new Hindernis(200, 400)); //hier wird die map/das feld initialisiert
          i = 0;
        }
      }


      addObject(neu);

      System.out.println("neues up");
    }

  }

  public void checkColl() {
    for (BewegtesObjekt obj : moving) {
      if (obj instanceof Projektil) {
        for (Line2D l : borders) {
          if (l.ptLineDist(obj.getXPos(), obj.getYPos()) < 10) collideProjektilBorder((Projektil) obj);
        }
      }
      for (ObjektMitHitbox omh : objects) {
        if (obj.equals(omh)) break;
        if (collides(obj.getHitbox(), omh.getHitbox())) {
          if (obj instanceof Panzer) { //wenn kollidierendes Objekt Panzer ist...
            if (omh instanceof Panzer)
              collidePanzerPanzer((Panzer) obj, (Panzer) omh);  //...und anderes Panzer ist wird diese Methode ausgeführt
            if (omh instanceof Projektil)
              collidePanzerProjektil((Panzer) obj, (Projektil) omh); //...und anderes Projektil ist ||
            if (omh instanceof Hindernis)
              collidePanzerHindernis((Panzer) obj, (Hindernis) omh); //...und anderes Hindernis ist ||
          }
          if (obj instanceof Projektil) { //ich glaube das Prinzip ist verstanden
            if (omh instanceof Panzer) collidePanzerProjektil((Panzer) omh, (Projektil) obj);
            if (omh instanceof Projektil) collideProjektilProjektil((Projektil) obj, (Projektil) omh);
            if (omh instanceof Hindernis) collideProjektilHindernis((Projektil) obj, (Hindernis) omh);
            if (omh instanceof Powerup) collideProjektilPowerup((Projektil) obj, (Powerup) omh);
          }
        }
      }

      if (obj instanceof Panzer) {
        for (Line2D l : borders) {
          for (Line2D l1 : getLinesFromHitbox(obj.getHitbox())) {
            if (l1.intersectsLine(l)) {
              if (collidePanzerBorder((Panzer) obj))
                break; //da alle 4 Linien der Hitbox getestet werden, muss nach einer kollision abgebrochen werden...
            }
          }
        }
      }


    }
    objects.removeAll(destroyed); //alle zerstörten objekte werden von den Spielobjekten entfernt
    moving.removeAll(movdestroyed); //alle zerstörten bewegten Objekte werden von den beegten Spielobjekten entfernt
    destroyed = new ArrayList<>(); //Liste wird zurückgesetzt
    movdestroyed = new ArrayList<>(); //Liste wird zurückgesetzt
  }


  private void collidePanzerPanzer(Panzer p1, Panzer p2) {
    // Teilgeschwindigkeiten beider Panzer auf beiden Achsen

    System.out.println(p1.getHp() + " " + p2.getHp());

    double vx1 = p1.getGeschwindigkeit() * Math.sin(p1.getRichtung());
    double vy1 = p1.getGeschwindigkeit() * Math.cos(p1.getRichtung());
    double vx2 = p2.getGeschwindigkeit() * Math.sin(p2.getRichtung());
    double vy2 = p2.getGeschwindigkeit() * Math.cos(p2.getRichtung());

    // Geschwindigkeit, mit der beide Panzer sich annähern (max. 2*vmax) multipliziert mit beliebigem Faktor

    int dmg = (int) (Math.sqrt(Math.pow(Math.abs(vx1 - vx2), 2) + Math.pow(Math.abs(vy1 - vy2), 2)) * GUI.getnColldmg());
    System.out.println(dmg);
    // Anrechnen des Schadens

    if (p1.nimmSchaden(dmg)) {
      System.out.println("p1 destroyed");
      p2.getFahrer().addPunkte(1);
      spawn(p1);
    }
    if (p2.nimmSchaden(dmg)) {
      System.out.println("p2 destroyed");
      p1.getFahrer().addPunkte(1);
      spawn(p2);
    }
    p1.zuruecksetzen();
    p2.zuruecksetzen();
  }

  private void collidePanzerHindernis(Panzer p, Hindernis h) {

    System.out.println(p.getGeschwindigkeit());
    if (p.nimmSchaden((int) (Math.abs(p.getGeschwindigkeit() * GUI.getnColldmg())))) {
      System.out.println("tot");
      spawn(p);
    }
    if (h.nimmSchaden((int) (Math.abs(p.getGeschwindigkeit() * GUI.getnColldmg())))) {
      destroyed.add(h);
    }
    p.zuruecksetzen();
  }

  private void collidePanzerProjektil(Panzer p, Projektil pew) {
    if (!pew.getSchuetze().equals(p.getFahrer())) {
      if (p.nimmSchaden(pew.getSchaden())) { //Panzer nimmt Schaden wenn von pew getroffen(mimentan ist schadenzahl typ von pew)
        pew.getSchuetze().addPunkte(1);
        spawn(p);
      }
      destroyed.add(pew); //pew wird bei Aufprall zerstört
      movdestroyed.add(pew);
    }
  }

  private void collideProjektilProjektil(Projektil pew1, Projektil pew2) {
    destroyed.add(pew1);
    destroyed.add(pew2);
    movdestroyed.add(pew1);
    movdestroyed.add(pew2);
    //Projektile werden zerstört
  }

  private void collideProjektilHindernis(Projektil pew, Hindernis h) {
    if (h.nimmSchaden(pew.getSchaden())) {
      destroyed.add(h);
    }
    destroyed.add(pew);//pew wird bei Aufprall zerstört
    movdestroyed.add(pew);
  }

  private boolean collidePanzerBorder(Panzer p) {
    boolean collided = false;


    if (p.nimmKollSchaden(GUI.getnColldmg())) {
      spawn(p);
      collided = true; //...daher wird bei einer Borderkollision true zurückgegeben, damit abgebrochen wird
    }
    p.zuruecksetzen();
    System.out.println(p.getFahrer().getSpawnX() + " " + p.getFahrer().getSpawnY());
    return collided;
  }

  private void collideProjektilBorder(Projektil pew) {
    System.out.println("pewpewtot");
    destroyed.add(pew);
    movdestroyed.add(pew);
  }

  private void collidePanzerPowerup(Panzer p, Powerup up) {
    System.out.println("Panzerpowerups");
    up.setPanzer(p);
    addPowerup(up);
    destroyed.add(up);
  }

  private void collideProjektilPowerup(Projektil pew, Powerup up) {
    destroyed.add(up);
    addPowerup(up);
    up.setPanzer(pew.getSchuetze().getPanzer());
  }

  public List<ObjektMitHitbox> getObjects() {
    return objects;
  }

  public void setObjects(List<ObjektMitHitbox> obj) {
    objects = obj;
    for (ObjektMitHitbox o : obj) {
      if (o instanceof BewegtesObjekt) {
        moving.add((BewegtesObjekt) o);
      }
      if (o instanceof Projektil) {
        projektils.add((Projektil) o);
      }
    }
  }

  public List<BewegtesObjekt> getMovingObjects() {
    respawn();
    return moving;
  }

  public void addObject(ObjektMitHitbox obj) {
    objects.add(obj);
    if (obj instanceof BewegtesObjekt) {
      moving.add((BewegtesObjekt) obj);
    }
    if (obj instanceof Projektil) {
      projektils.add((Projektil) obj);
    }
  }


  private void addPowerup(Powerup p) {
    powerups.add(p);
  }

  private boolean collides(Shape s1, Shape s2) { //hitboxen der spielobjekte werden übergeben
    Area a1 = new Area(s1);
    a1.intersect(new Area(s2)); //a1 wird der schnittfläche der hitboxen gleichgesetzt
    return !a1.isEmpty(); //wenn die schnittfläche nicht leer ist, wird 'true' übergeben
  }

  private void respawn() {
    List<Panzer> hilfSpawn = new ArrayList<>();
    for (Panzer p : spawn) {
      if (p.getFahrer().getRespawn() <= System.currentTimeMillis()) { //respawn mus verbessert werden, funktioniert nicht riochtig
        p.getFahrer().setPanzer(p); //sowie der panzer wieder sichtabr wird(also respawnt) wird seinem fahrer die kontrolle darüber gegeben
        hilfSpawn.add(p);
        objects.add(p);
        moving.add(p);
        p.getFahrer().setPanzer(p);
        p.getFahrer().setRespawn(0);
      }
    }
    spawn.removeAll(hilfSpawn);
  }


  private void spawn(Panzer p) {
    destroyed.add(p);
    movdestroyed.add(p);
    Panzer neu = new Panzer(p.getMasse(), p.getFahrer().getSpawnX(), p.getFahrer().getSpawnY(), p.getFahrer());
    p.getFahrer().addPunkte(-1);
    p.getFahrer().setRespawn((System.currentTimeMillis() + Main.RESPAWN));
    spawn.add(neu);
  }

  private Line2D[] getLinesFromHitbox(Shape s) { //gibt die Außenlinien einer H(obb)itbox zurück
    PathIterator iterator = s.getPathIterator(null);
    Line2D[] lines = new Line2D[4];
    java.util.List<Point2D> points = new ArrayList<>();
    double[] coords = new double[2];
    while (!iterator.isDone()) {
      if (iterator.currentSegment(coords) != PathIterator.SEG_CLOSE) {
        points.add(new Point2D.Double(coords[0], coords[1])); //spiechert alle Eckpunkte der SHape
      }
      iterator.next();
    }
    points.set(4, points.get(0)); //trick für die for-loop
    for (int i = 0; i < 4; i++) {
      lines[i] = new Line2D.Double(points.get(i), points.get(i + 1)); //die eckpunkte werden in linien umgewandelt
    }
    return lines; //Linien werden in einem Array zurückgegeben;
  }


}

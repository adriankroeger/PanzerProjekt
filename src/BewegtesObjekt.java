/*
Oberklasse für alle beweglichen Objekte; Unterklasse von ObjektMitHitbox
verfasst von Niklas Schlüter

Aufgrund der verschiedenen Formen und Größen der gespeicherten Objekte muss
die Hitbox wann immer nötig von einer Unterklasse selbst definiert werden.
Das Abspeichern von Geschwindigkeit und Richtung erfolgt hier und lässt eine
übergeordnete Methode für das Verschieben der Koordinaten zu.
*/

import java.awt.geom.AffineTransform;

public class BewegtesObjekt extends ObjektMitHitbox {

  // Attribute
  protected double v;  // aktuelle Geschwindigkeit, d.h. pro Tick zurückgelegte Gesamtstrecke
  protected double r;     // Ausrichtung in Grad relativ zu einer Senkrechten des Spielfeldes, im Uhrzeigersinn fortschreitend

  // Konstruktor
  public BewegtesObjekt(double posx, double posy, double geschwindigkeit, double richtung) {
    super(posx, posy);
    v = geschwindigkeit;
    r = richtung;
  }

  // Aktualisiert die Position, d.h. berechnet die Verschiebung in x- und y-Richtung beim Zurücklegen
  // der Strecke v im derzeitigen Winkel r und addiert sie auf die vorherigen Koordinaten; aktualisiert
  // die Hitbox dementsprechend
  protected void bewege() {
    double xdiff = Math.sin(r) * v;
    double ydiff = -Math.cos(r) * v;
    xpos += xdiff;
    ypos += ydiff;
    AffineTransform t = new AffineTransform();
    t.translate(xdiff, ydiff);
    hb = t.createTransformedShape(hb);
  }

  // get- und set-Methoden:
  public double getRichtung() {
    return r;
  }

  public double getGeschwindigkeit() {
    return v;
  }

}  
import javax.imageio.ImageIO;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;

/*
 * Erstellt von Adrian
 */
public class Hindernis extends ObjektMitHitbox {

  private final int hpmax;
  private int hp;
  private String respath = System.getProperty("user.dir").replace("PanzerProjekt","").replace("\\src", "") + "\\PanzerProjekt\\resources\\";

  public Hindernis(double posX, double posY) {
    super(posX, posY);
    hpmax = GUI.getnHphind();
    hp = hpmax;
    hb = new Rectangle2D.Double(posX, posY, 40, 40);
    //this.typ = typ;
  }

  public boolean nimmSchaden(int dmg) {
    hp -= dmg;
    return hp < 1;
  }

  public BufferedImage getImage() throws Exception{ //Je nach Beschädigungszustand des Hindernisses wird ein anderes Bild übergeben, welches angezeigt werden soll
    if (hp <= hpmax * 0.25) return ImageIO.read(new File(respath + "Hindernis_3.png"));

    if (hp <= hpmax * 0.50) return ImageIO.read(new File(respath + "Hindernis_2.png"));

    if (hp <= hpmax * 0.75) return ImageIO.read(new File(respath + "Hindernis_1.png"));

    return ImageIO.read(new File(respath + "Hindernis_0.png"));
  }


}

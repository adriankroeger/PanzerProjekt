import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class GUI extends JFrame {

  // diverse Normwerte
  private static double nVmax = 5;
  private static double nVpmult = 5;
  private static int nAcc = 25;
  private static int nColldmg = 100;
  private static int nAmmo = 99;
  private static int nHpmult = 250;
  private static int nCdmax = 50;
  private static int nHphind = 1000;
  // vom Spieler im Menü eingegebene und für jeden Spieler individuelle Voreinstellungen
  private JTextField[] spielerwerte = new JTextField[8];

  private String respath = System.getProperty("user.dir").replace("PanzerProjekt","").replace("\\src", "") + "\\PanzerProjekt\\resources\\"; //Falls der Projektordner bei Ihnen anders heißt, bitte "PanzerProjekt" dadurch ersetzen
  //Im (ganz ganz) seltenen Falle, dass dieser Pfad inkorrekt ist, den Pfad zum Ordner "resources" bitte hardcoden
  private ImageIcon tank = new ImageIcon(respath + "tank.png");
  private Runner runner = new Runner();
  private Panzer p = new Panzer(2, 2, 0, new Spieler(1, 0, 0));
  private Map<Integer, Spieler> spielers = new HashMap<>();
  private Map<Integer, Boolean> ausrollendeSpielers = new HashMap<>();
  private List<JLabel> labels = new ArrayList<>();
  private Character[][] tastenbelegung = new Character[][]{
          {'w', 's', 'a', 'd', 'c'},
          {'z', 'h', 'g', 'j', 'm'},
          {'p', 'ö', 'l', 'ä', '-'},
          {'8', '5', '4', '6', '3'}
  };

  public GUI() {
    initGUI();  //  Initialisiert das Fenster mit allen seinen Grundeigenschaften.
    main();
  }

  // getter für Normwerte
  public static double getnVmax() {
    return nVmax;
  }

  public static double getnVpmult() {
    return nVpmult;
  }

  public static int getnAcc() {
    return nAcc;
  }

  public static int getnColldmg() {
    return nColldmg;
  }

  public static int getnAmmo() {
    return nAmmo;
  }

  public static int getnHpmult() {
    return nHpmult;
  }

  public static int getnHphind() {
    return nHphind;
  }

  public static int getnCdmax() {
    return nCdmax;
  }

  private void initGUI() {
    setLayout(null);
    setIconImage(tank.getImage());  //  Setzt das kleine Icon in der oberen linken Ecke.
    setTitle("PanzerProjekt");  //  Titel wird oben am Fenster angezeigt
    setSize(Main.WIDTH, Main.HEIGHT);  //  setzt die Groesse des Fensters am Anfang
    setLocationRelativeTo(null);    //  zentriert die Startposition
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);    //  Durch X kann man das Fenster schliessen
  }

  private void main() {
    setVisible(true);   //  Fenster wird sichtbar.
    runner.runmenu();
  }

  public void changeKeys(int player, int action, Character newkey) {
    tastenbelegung[player][action] = newkey;
  }

  public void changeSpielers(int id) {
    if (spielers.containsKey(id)) {
      ausrollendeSpielers.remove(id);
      spielers.remove(id);
    } else {
      ausrollendeSpielers.put(id, true);
      switch (id) {
        case 0:
          spielers.put(id, new Spieler(id, 100, 100));
          break;
        case 1:
          spielers.put(id, new Spieler(id, 1400, 100));
          break;
        case 2:
          spielers.put(id, new Spieler(id, 100, 900));
          break;
        case 3:
          spielers.put(id, new Spieler(id, 1400, 900));
          break;
      }
    }
  }


  private class Runner extends Thread {
    private boolean shouldStop = false;
    private boolean pause = false;
    private boolean inMenu = true;

    private boolean inSettings = false; // gibt an ob die Settingsloop laeuft
    private boolean settingsPanelRunning = false; // gibt an ob das Settingspanel laeuft

    public void shouldStop() {
      this.shouldStop = true;
    }   //  stoppt die Anzeigeloop

    public void pauseGame() {
      this.pause = true;
    }   // pausiert die Anzeigeloop

    public void resumeGame() {
      this.pause = false;
    }   // entpausiert die Anzeigeloop

    private void runmenu() {
      MainMenu menu = new MainMenu();
      Settings settings = null;
      add(menu);
      repaint();

      while (inMenu) {
        while (inSettings) {

          if (!settingsPanelRunning) {
            // disabled alle Tastenbelegungsbuttons, alle anderen Komponenten des Menues bleiben funktionstuechtig
            for (Component i : menu.getComponents()) {
              if (i instanceof TastenbelegungsButton) {
                i.setEnabled(false);
              }

            }

            settings = new Settings();
            add(settings);

            settingsPanelRunning = true;
            settings.repaint();
          }

          try {
            Thread.sleep(33);   // Grafiken werden alle 33ms geladen. ca. 30Hz
            settings.repaint();

          } catch (InterruptedException e) {
            e.printStackTrace();
          }


          if (!inSettings) {
            for (Component i : menu.getComponents()) {
              if (i instanceof TastenbelegungsButton) {
                i.setEnabled(true);
              }

            }
            settings.aktualisiere();
            settingsPanelRunning = false;
            remove(settings);
            repaint();
            System.out.println("settingspanel gekillt");
          }
        }

        try {
          Thread.sleep(1);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }

      remove(menu);
      repaint();
      revalidate();
      run();
    }

    private void stopMenu() {
      stopSettings();
      inMenu = false;
    }

    private void startorkillSettings() {
      inSettings = !inSettings;
    }

    private void stopSettings() {
      inSettings = false;
    }

    public void run() {
      guiFeld spielfeld = new guiFeld();
      add(spielfeld);


      MultiKeyPressListener gameListener = new MultiKeyPressListener();
      int i = 0;
      for (Map.Entry<Integer, Spieler> spieler : spielers.entrySet()) {
        System.out.println(i);
        Panzer panzer = new Panzer(Integer.parseInt(spielerwerte[1 + i * 2].getText()), spieler.getValue().getSpawnX(), spieler.getValue().getSpawnY(), spieler.getValue());
        System.out.println(panzer.getXPos());
        System.out.println(panzer.getHitbox().toString());
        spieler.getValue().setPanzer(panzer);
        spieler.getValue().setName(spielerwerte[i * 2].getText());
        Main.getFeld().addObject(panzer);
        i++;
      }


      spielfeld.addKeyListener(gameListener);
      spielfeld.grabFocus();
      spielfeld.setSize(Main.WIDTH, Main.HEIGHT);
      inMenu = false;
      GUI.this.setSize(Main.WIDTH + 200, Main.HEIGHT); //Dschui wird um das statsPanel vergößert
      Stats stats = new Stats();
      add(stats);
      while (!shouldStop) {
        try {
          Thread.sleep(20);   // Grafiken werden alle 20ms geladen. ca. 50Hz
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        if (!pause) {

          Main.getFeld().update();
          Main.getFeld().checkColl();
          for (JLabel label : labels) {
            spielfeld.remove(label); //alle existierenden labels werden entfernt, um die neuen anzuzeigen
          }

          labels = new ArrayList<>(); //labels wird geleert

          for (ObjektMitHitbox object : Main.getFeld().getObjects()) {
            if (object instanceof Panzer) {
              labels.add(new TankLabel((Panzer) object)); //labels wird mit allen anzuzeigenden objekten gefüllt
            }
            if (object instanceof Projektil) {
              labels.add(new ProjektilLabel((Projektil) object));
            }

            if (object instanceof Hindernis) {
              labels.add(new HindernisLabel((Hindernis) object));

            }
            if (object instanceof Powerup) {
              labels.add(new PowerupLabel((Powerup)object));
            }
          }

          for (JLabel label : labels) {
            spielfeld.add(label);
          }


          // Es wird angenommen, dass alle Spieler ausrollen/nicht fahren.
          for (Integer ausrollenderspieler : ausrollendeSpielers.keySet()) {
            ausrollendeSpielers.put(ausrollenderspieler, true);
          }


          for (Character key : gameListener.getGedrueckt()) {
            for (Map.Entry<Integer, Spieler> spieler : spielers.entrySet()) {
              for (int j = 0; j < 5; j++) {
                if (key == (char) tastenbelegung[spieler.getKey()][j]) {
                  switch (j) {
                    case 0:
                      spieler.getValue().vorwaerts();
                      ausrollendeSpielers.put(spieler.getKey(), false);
                      break;
                    case 1:
                      spieler.getValue().rueckwaerts();
                      ausrollendeSpielers.put(spieler.getKey(), false);
                      break;
                    case 2:
                      spieler.getValue().linksDrehen();
                      break;
                    case 3:
                      spieler.getValue().rechtsDrehen();
                      break;
                    case 4:
                      spieler.getValue().feure();
                      break;
                  }
                }
              }
            }
          }

          //Alle Spieler die nicht fahren, rollen aus
          for (Map.Entry<Integer, Spieler> spieler : spielers.entrySet()) {
            if (ausrollendeSpielers.get(spieler.getKey())) {
              spieler.getValue().rollen();
            }
          }


          for (Component comp : spielfeld.getComponents()) {    //Bitte in n Switch case umaendern
            if (comp instanceof TankLabel) {
              comp.setLocation(((TankLabel) comp).getPanzer().getXPos() - 46, ((TankLabel) comp).getPanzer().getYPos() - 68);
            }
            if (comp instanceof ProjektilLabel) {
              comp.setLocation(((ProjektilLabel) comp).getProjektil().getXPos() - 21, ((ProjektilLabel) comp).getProjektil().getYPos() - 44);
            }
            if (comp instanceof HindernisLabel) {
              comp.setLocation(((HindernisLabel) comp).getHindernis().getXPos() - 7, ((HindernisLabel) comp).getHindernis().getYPos() - 29);
            }
            if(comp instanceof PowerupLabel){
              comp.setLocation(((PowerupLabel) comp).getPowerup().getXPos() - 7, ((PowerupLabel) comp).getPowerup().getYPos() - 29);
            }
          }
          spielfeld.repaint();
          stats.aktualisiere();
        }
      }
    }

    public boolean isPaused() {
      return pause;
    }   //  Gibt an ob die Anzeigeloop pausiert ist.
  }

  class MultiKeyPressListener implements KeyListener {

    private final Set<Character> gedrueckt = new HashSet<>();


    public Set<Character> getGedrueckt() {
      return gedrueckt;
    }


    @Override
    public synchronized void keyPressed(KeyEvent e) {
      gedrueckt.add(e.getKeyChar());
    }

    @Override
    public synchronized void keyReleased(KeyEvent e) {
      gedrueckt.remove(e.getKeyChar());
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }
  }

  private class guiFeld extends JPanel {
    private guiFeld() {
      setLayout(null);
      setBackground(Color.lightGray);
      setBounds(0, 0, 800, 600);

    }

  }

  private class MainMenu extends JPanel {
    InputMap im = getInputMap(WHEN_IN_FOCUSED_WINDOW);
    ActionMap am = getActionMap();


    PlayButton playbutton = new PlayButton();
    private BufferedImage menuimg;

    private MainMenu() {
      setLayout(null);
      setBackground(Color.green);

      try {
        menuimg = ImageIO.read(new File(respath + "Menu.png")); //Bild des Panzers wird als BufferedImage aus der Datei gelesen und uebergeben.
        setBounds(0, 0, menuimg.getWidth(), menuimg.getHeight());    //Das JLabel bekommt die Groesse des Bildes.
      } catch (IOException e) {
        e.printStackTrace();
      }

      int x = 70;
      int y = 140;
      for (int i = 0; i < 20; i++) {
        if (i % 5 == 0) {
          y += 65;
          x = 80;
        }
        x += 120;
        add(new TastenbelegungsButton(x, y, i));
      }

      // fügt TextFields für die Eingabe der Spielernamen hinzu
      for (int i = 0; i < 8; i += 2) {
        spielerwerte[i] = new JTextField("Spieler " + (1 + i / 2));
        spielerwerte[i].setBounds(731, 205 + 33 * i, 69, 25);
        add(spielerwerte[i]);
      }
      // fügt TextFields für die Eingabe gewünschter Gewichtsklassen hinzu
      for (int i = 1; i < 8; i += 2) {
        spielerwerte[i] = new JTextField("2");
        spielerwerte[i].setBounds(731, 230 + 33 * (i - 1), 69, 25);
        add(spielerwerte[i]);
      }

      for (int i = 0; i < 4; i++) {
        add(new SpielerButton(100, 140 + i * 65 + 65, i));
      }

      add(new SettingsButton());

      // Startet bei Leertaste das Spiel
      im.put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), "space");
      am.put("space", new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent e) {
          runner.stopMenu();
        }
      });

      add(playbutton);
      GUI.this.repaint();
    }

    @Override   //  Uberschreibt die Methode des JLabels, die letzendlich die Grafik aufs Fenster malt.
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      g.drawImage(menuimg, 0, 0, this); //  Bild des Menus wird aufs JLabel platziert.
    }

  }

  private class Settings extends JPanel {
    private JSlider slVmax = new JSlider(10, 200, (int) (nVmax * 10));
    private JSlider slVpmult = new JSlider(15, 100, (int) (nVpmult * 10));
    private JSlider slAcc = new JSlider(10, 60, nAcc);
    private JSlider slColldmg = new JSlider(0, 1000, nColldmg);
    private JSlider slAmmo = new JSlider(1, 999, nAmmo);
    private JSlider slHpmult = new JSlider(1, 2000, nHpmult);
    private JSlider slCdmax = new JSlider(1, 200, nCdmax);
    private JSlider slHphind = new JSlider(1, 2500, nHphind);
    private JLabel[] lbnames = new JLabel[8];
    private String[] names = new String[]{"Höchstgeschwindigkeit", "Multiplikator für Projektilgeschwindigkeit", "Trägheit der Panzer",
            "Kollisionsschaden", "Munitionsmenge", "HP der Panzer", "Cooldown nach Feuern", "HP der Hindernisse"};

    private Settings() {
      setLayout(null);
      setBackground(Color.green);
      setBounds(150, 100, 500, 350);
      slVmax.setBounds(25, 50, 200, 50);
      slVpmult.setBounds(25, 125, 200, 50);
      slAcc.setBounds(25, 200, 200, 50);
      slColldmg.setBounds(25, 275, 200, 50);
      slAmmo.setBounds(275, 50, 200, 50);
      slHpmult.setBounds(275, 125, 200, 50);
      slCdmax.setBounds(275, 200, 200, 50);
      slHphind.setBounds(275, 275, 200, 50);
      add(slVmax);
      add(slVpmult);
      add(slAcc);
      add(slColldmg);
      add(slAmmo);
      add(slHpmult);
      add(slCdmax);
      add(slHphind);
      for (int i = 0; i < 8; i++) {
        lbnames[i] = new JLabel(names[i]);
        add(lbnames[i]);
      }
      for (int i = 0; i < 4; i++) lbnames[i].setBounds(25, 25 + i * 75, 200, 25);
      for (int i = 4; i < 8; i++) lbnames[i].setBounds(275, i * 75 - 275, 200, 25);
    }

    @Override   //  Uberschreibt die Methode des JLabels, die letzendlich die Grafik aufs Fenster malt.
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
    }

    public void aktualisiere() {
      nVmax = slVmax.getValue() / 10;
      nVpmult = slVpmult.getValue() / 10;
      nAcc = slAcc.getValue();
      nColldmg = slColldmg.getValue();
      nAmmo = slAmmo.getValue();
      nHpmult = slHpmult.getValue();
      nCdmax = slCdmax.getValue();
    }

  }

  // Anzeige diverser Werte der Spieler und ihrer Panzer am rechten Spielfeldrand
  private class Stats extends JPanel {
    // Labels zur Anzeige diverser absoluter Werte inklusive Beschriftung
    private JLabel[] lbcpt = new JLabel[4];  // "Caption", zeigt Spielernamen und Punkte
    private JLabel[] lbv = new JLabel[4];    // Geschwindigkeit
    private JLabel[] lbhp = new JLabel[4];   // Health Points
    private JLabel[] lbammo = new JLabel[4]; // Munition
    private JLabel[] lbcd = new JLabel[4];   // Cooldown
    // ProgressBars zur Anzeige der Werte relativ zum Maximalwert
    private JProgressBar[] pbv = new JProgressBar[4];    // siehe oben
    private JProgressBar[] pbhp = new JProgressBar[4];
    private JProgressBar[] pbammo = new JProgressBar[4];
    private JProgressBar[] pbcd = new JProgressBar[4];

    private Stats() {
      setLayout(null);
      setBounds(Main.WIDTH, 0, 200, 600);
      setBackground(Color.yellow); //markante Farbe für testzwecke
      for (int i = 0; i < 4; i++) {
        // Initialisierung des Labels
        lbcpt[i] = new JLabel();
        // Setzen an die korrekte Position
        lbcpt[i].setBounds(5, 150 * i, 200, 40);
        lbv[i] = new JLabel();
        lbv[i].setBounds(5, 50 + 150 * i, 100, 20);
        lbhp[i] = new JLabel();
        lbhp[i].setBounds(5, 75 + 150 * i, 100, 20);
        lbammo[i] = new JLabel();
        lbammo[i].setBounds(5, 100 + 150 * i, 100, 20);
        lbcd[i] = new JLabel();
        lbcd[i].setBounds(5, 125 + 150 * i, 100, 20);
        pbv[i] = new JProgressBar();
        pbv[i].setBounds(100, 50 + 150 * i, 100, 20);
        pbhp[i] = new JProgressBar();
        pbhp[i].setBounds(100, 75 + 150 * i, 100, 20);
        pbammo[i] = new JProgressBar();
        pbammo[i].setBounds(100, 100 + 150 * i, 100, 20);
        pbcd[i] = new JProgressBar();
        pbcd[i].setBounds(100, 125 + 150 * i, 100, 20);
        // Wert wird überschrieben, sollte doch ein Spieler vorhanden sein
        lbcpt[i].setText("kein Spieler");
        add(lbcpt[i]);
        add(lbv[i]);
        add(lbhp[i]);
        add(lbammo[i]);
        add(lbcd[i]);
        add(pbv[i]);
        add(pbhp[i]);
        add(pbammo[i]);
        add(pbcd[i]);
      }
      GUI.this.repaint();
    }

    // Aktualisiert jeden Tick die angezeigten Werte
    // siehe auch Kommentare zur Methode werteAnzeigen() in der Panzer-Klasse
    private void aktualisiere() {
      for (int i = 0; i < spielers.size(); i++) {
        int[] werte = spielers.get(i).getPanzer().werteAnzeigen();
        lbv[i].setText((double) werte[0] / 10 + " px/sec");
        lbhp[i].setText(werte[2] + " HP");
        lbammo[i].setText(werte[4] + " Schuss");
        lbcd[i].setText((double) werte[6] / 10 + "  sec CD");
        pbv[i].setValue(werte[1]);
        pbhp[i].setValue(werte[3]);
        pbammo[i].setValue(werte[5]);
        pbcd[i].setValue(werte[7]);
        lbcpt[i].setText(spielers.get(i).getName() + ": " + spielers.get(i).getPunkte() + " Pt.");
      }
    }
  }

  private class TastenbelegungsButton extends JButton {
    private BufferedImage tasteonimg; //ToDo
    private BufferedImage tasteoffimg; //ToDo
    private BufferedImage presskeyimg; //ToDo

    private boolean mouseover = false;
    private boolean changing = false;

    private int x;
    private int y;

    private int id;

    private KeyListener changeKey = new KeyListener() {
      @Override
      public void keyTyped(KeyEvent e) {
      }

      @Override
      public void keyPressed(KeyEvent e) {
        changeKeys(id / 5, id % 5, e.getKeyChar());
        setText(Character.toString(tastenbelegung[id / 5][id % 5]));
        System.out.println(e.getKeyCode());
      }

      @Override
      public void keyReleased(KeyEvent e) {
        setFocusable(false);
        removeKeyListener(this);
        setFocusable(true);
        changing = false;
      }
    };

    private TastenbelegungsButton(int x, int y, int id) {
      this.x = x;
      this.y = y;
      this.id = id;


      setBounds(x, y, 50, 50);    //Das JLabel bekommt die Groesse des Bildes.
      setFont(new Font("Arial", Font.PLAIN, 15));
      setText(Character.toString(tastenbelegung[id / 5][id % 5]));

      addMouseListener(new MouseAdapter() {
        @Override
        public void mouseEntered(MouseEvent e) {
          super.mouseEntered(e);
          setMouseover(true);

        }

        @Override
        public void mouseExited(MouseEvent e) {
          super.mouseExited(e);
          setMouseover(false);
        }
      });

      addActionListener(e -> changeTaste());

    }

    protected void changeTaste() {
      changing = true;
      addKeyListener(changeKey);
    }

    @Override
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      if (mouseover || changing) {
        //setText(Integer.toString(tastenbelegung[id/5][id%5]));
        setBackground(Color.LIGHT_GRAY);

      } else {
        setBackground(Color.GRAY);
      }
    }

    protected void setMouseover(boolean x) {
      mouseover = x;
    }

  }

  private class PlayButton extends JButton {
    private BufferedImage onimg;
    private BufferedImage offimg;
    private boolean mouseover = false;


    private PlayButton() {
      try {
        onimg = ImageIO.read(new File(respath + "PlaybuttonOn.png"));
        offimg = ImageIO.read(new File(respath + "PlaybuttonOff.png"));

        setBounds(100, 450, offimg.getWidth(), offimg.getHeight());    //Das JLabel bekommt die Groesse des Bildes.
      } catch (IOException e) {
        e.printStackTrace();
      }

      setBorderPainted(false); // Verhindert, dass blaue Raender gezeichnet werden.

      addMouseListener(new MouseAdapter() {
        @Override
        public void mouseEntered(MouseEvent e) {
          super.mouseEntered(e);
          setMouseover(true);
        }

        @Override
        public void mouseExited(MouseEvent e) {
          super.mouseExited(e);
          setMouseover(false);
        }
      });
      addActionListener(e -> runner.stopMenu());
    }

    @Override   //  Uberschreibt die Methode des JLabels, die letzendlich die Grafik aufs Fenster malt.
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);

      if (mouseover) {
        g.drawImage(onimg, 0, 0, this); //  Bild des Menus wird aufs JLabel platziert.
      } else {
        g.drawImage(offimg, 0, 0, this); //  Bild des Menus wird aufs JLabel platziert.
      }

    }

    protected void setMouseover(boolean x) {
      mouseover = x;
    }

  }

  private class SpielerButton extends JButton {
    private BufferedImage spielerbtnon;
    private BufferedImage spielerbtnoff;

    private boolean mouseover = false;
    private boolean activated = false;

    private int x;
    private int y;

    private int id;

    private SpielerButton(int x, int y, int id) {
      this.x = x;
      this.y = y;
      this.id = id;

      setBounds(x, y, 50, 50);    //Das JLabel bekommt die Groesse des Bildes.


      addActionListener(e -> clicked());
    }

    private void clicked() {
      activated = !activated;
      changeSpielers(id);
    }

    @Override   //  Uberschreibt die Methode des JLabels, die letzendlich die Grafik aufs Fenster malt.
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);

      if (mouseover && !activated) {
        setBackground(Color.LIGHT_GRAY);
      } else if (!mouseover && !activated) {
        setBackground(Color.GRAY);
      } else {
        setBackground(Color.green);
      }

    }
  }

  private class SettingsButton extends JButton {
    private BufferedImage settingsimage;

    private SettingsButton() {
      setSize(50, 50);
      addActionListener(e -> runner.startorkillSettings());
    }


  }

  private class TankLabel extends JLabel {
    private BufferedImage panzerimg;

    private Panzer panzerobj;

    private TankLabel(Panzer panzerobj) {

      this.panzerobj = panzerobj;
      try {
        panzerimg = ImageIO.read(new File(respath + "tank.png")); //Bild des Panzers wird als BufferedImage aus der Datei gelesen und uebergeben.
        setSize(79, 79);   //Das JLabel bekommt die Groesse des Bildes.
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    private Panzer getPanzer() {
      return panzerobj;
    }

    @Override   //  Uberschreibt die Methode des JLabels, die letzendlich die Grafik aufs Fenster malt.
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D) g;
      g2.rotate(panzerobj.getRichtung(), getWidth() / 2, getHeight() / 2);
      g.drawImage(panzerimg, 14, 9, this); //  Bild des Panzers wird aufs JLabel platziert.
    }
  }


  private class ProjektilLabel extends JLabel {
    private BufferedImage bulletimg1;
    private BufferedImage bulletimg2;
    private BufferedImage bulletimg3;

    private Projektil projektilobj;

    private ProjektilLabel(Projektil projektilobj) {
      this.projektilobj = projektilobj;
      try {
        bulletimg1 = ImageIO.read(new File(respath + "Bullet1.png"));
        bulletimg2 = ImageIO.read(new File(respath + "Bullet2.png"));
        bulletimg3 = ImageIO.read(new File(respath + "Bullet3.png"));
        setBounds(0, 0, bulletimg1.getWidth(), bulletimg1.getHeight());
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    private Projektil getProjektil() {
      return projektilobj;
    }

    @Override   //  Uberschreibt die Methode des JLabels, die letzendlich die Grafik aufs Fenster malt.
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      switch (getProjektil().getTyp()){
        case 1: g.drawImage(bulletimg1, 0, 0, this);break;
        case 2: g.drawImage(bulletimg2, 0, 0, this);break;
        case 3: g.drawImage(bulletimg3, 0, 0, this);break;
      }
       //  Bild des Panzers wird aufs JLabel platziert.
    }
  }

  private class HindernisLabel extends JLabel {
    private Hindernis hindernisobj;

    private HindernisLabel(Hindernis hindernisobj) {
      this.hindernisobj = hindernisobj;
      setBounds(0, 0, 40, 40);
    }

    private Hindernis getHindernis() {
      return hindernisobj;
    }

    @Override   //  Uberschreibt die Methode des JLabels, die letzendlich die Grafik aufs Fenster malt.
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);
      try {
        g.drawImage(hindernisobj.getImage(), 0, 0, this);
      }
      catch (Exception e){
        e.printStackTrace();
      }
      //setIcon(new ImageIcon(hindernisobj.getImage())); //Das Hindernis hat verschiedene Bilder für verschiedene Beschädigungsstufen, daher wird das anzuzeigende Bild vom Hindernis übergeben
    }

  }

  private class PowerupLabel extends JLabel{
    private Powerup powerupobj;

    private PowerupLabel(Powerup powerupobj){
      this.powerupobj = powerupobj;
      setBounds(0,0, 20,20);
    }

    private Powerup getPowerup(){
      return powerupobj;
    }

    @Override
    protected void paintComponent(Graphics g) {
      super.paintComponent(g);

      try{
        g.drawImage(powerupobj.getImage(), 0, 0, this);
      }
      catch(Exception e){
        e.printStackTrace();
      }
    }
  }

}